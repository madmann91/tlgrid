// Two level grid
typedef struct {
    float min[3];
    int dimx;
    float max[3];
    int dimy;
    float cell_size[3];
    int dimz;
    float inv_size[3];
    int pad;
} Grid;

// Cell in the structure
typedef struct {
    int begin;
    int dim;
} Cell;

// Leaf in the structure
typedef struct {
    int begin;
    int end;
} Leaf;

// Triangle, optimized to store the edges and the normal
typedef struct {
    float v0[3];
    float nx;
    float e1[3];
    float ny;
    float e2[3];
    float nz;
} Tri;

typedef struct {
    float org[3];
    float tmin;
    float dir[3];
    float tmax;
} Ray;

typedef struct {
    int tri_id;
    float t, u, v;
} Hit;

typedef struct {
    float3 tnext;
    float3 delta;
    int3 voxel;
    int3 dims;
} State;

inline float prodsign(float x, float y) {
    return as_float(as_int(x) ^ (as_int(y) & 0x80000000));
}

inline float safe_rcp(float x) {
    return (x != 0.0f) ? 1.0f / x : copysign(FLT_MAX, x);
}

inline float2 intersect_ray_box(float3 org, float3 inv_dir, float3 min, float3 max) {
    const float3 tmin = (min - org) * inv_dir;
    const float3 tmax = (max - org) * inv_dir;
    const float3 t0 = fmin(tmin, tmax);
    const float3 t1 = fmax(tmin, tmax);
    return (float2)(fmax(t0.x, fmax(t0.y, t0.z)),
                    fmin(t1.x, fmin(t1.y, t1.z)));
}

inline float4 intersect_ray_tri(float3 org, float3 dir, float tmin, float tmax,
                                float3 v0, float3 e1, float3 e2, float3 n) {
    const float3 c = v0 - org;
    const float3 r = cross(dir, c);
    const float det = dot(n, dir);
    const float abs_det = fabs(det);

    const float u = prodsign(dot(r, e2), det);
    const float v = prodsign(dot(r, e1), det);
    const float w = abs_det - u - v;

    if (u >= 0.0f && v >= 0.0f && w >= 0.0f) {
        const float t = prodsign(dot(n, c), det);
        if (det != 0.0f && t >= abs_det * tmin && abs_det * tmax >= t) {
            const float inv_det = 1.0f / abs_det;
            return (float4)(as_float(1), t * inv_det, u * inv_det, v * inv_det);
        }
    }

    return (float4)(as_float(0), 0.0f, 0.0f, 0.0f);
}

kernel void traverse_grid(const global Grid* grid,
                          const global Leaf* leaves,
                          const global Cell* cells,
                          const global int* tri_ids,
                          const global Tri* tris,
                          const global Ray* rays,
                          global Hit* hits,
                          int ray_count) {
    const int id = get_global_id(0);
    if (id >= ray_count)
        return;

    // Load the ray in a vectorized fashion
    const global float* ray_ptr = (const global float*)(rays + id);
    const float4 ray0 = vload4(0, ray_ptr);
    const float4 ray1 = vload4(1, ray_ptr);
    const float3 org = ray0.xyz;
    const float3 dir = ray1.xyz;
    const float tmin = ray0.w;
    const float tmax = ray1.w;

    const float3 inv_dir = (float3)(safe_rcp(dir.x),
                                    safe_rcp(dir.y),
                                    safe_rcp(dir.z));

    // Same for the grid
    const global float* grid_ptr = (const global float*)grid;
    const float4 grid0 = vload4(0, grid_ptr);
    const float4 grid1 = vload4(1, grid_ptr);
    const float4 grid2 = vload4(2, grid_ptr);
    const float4 grid3 = vload4(3, grid_ptr);
    const int3 grid_dims = (int3)(as_int(grid0.w),
                                  as_int(grid1.w),
                                  as_int(grid2.w));
    const float3 grid_min  = grid0.xyz;
    const float3 grid_max  = grid1.xyz;
    const float3 cell_size = grid2.xyz;
    const float3 grid_inv  = grid3.xyz;

    // Intersect the grid bounding box
    const float2 tbox = intersect_ray_box(org, inv_dir, grid_min, grid_max);
    const float tstart = fmax(tbox.x, tmin);
    const float tend = fmin(tbox.y, tmax);

    float t = tmax;
    float u = 0.0f;
    float v = 0.0f;
    int tri_id = -1;

    if (tstart > tend) goto end;

    // Initialize current state
    const float3 entry = tstart * dir + org;
    int3 voxel = clamp(convert_int3((entry - grid_min) * grid_inv), 0, grid_dims - 1);

    const int3 next_voxel = voxel + (dir > 0.0f ? 1 : 0);
    const float3 next = convert_float3(next_voxel) * cell_size + grid_min;
    float3 tnext = (next - org) * inv_dir;
    float tprev = tstart;

    const int3 vstep = dir > 0.0f ? 1 : -1;
    float3 delta = fabs(cell_size * inv_dir);

    // Outer loop
    while (true) {
        // Initialize sub-level state
        const int cell_id = voxel.x + grid_dims.x * (voxel.y + grid_dims.y * voxel.z);
        const int2 cell = vload2(0, (const global int*)(cells + cell_id));

        // Optimization for empty cells
        if (cell.y > 0) {
            // Save previous state
            const int3 old_voxel = voxel;
            const float3 old_tnext = tnext;

            const int cell_begin = cell.x;
            const int dim = cell.y;
            const float inv_dim = 1.0f / (float)dim;
            const float3 cell_inv = grid_inv * dim;
            const float3 cell_min = grid_min + cell_size * convert_float3(voxel);
            const float3 subentry = tprev * dir + org;
            const int3 subvoxel = clamp(convert_int3((subentry - cell_min) * cell_inv), 0, dim - 1);
            const int3 next_subvoxel = subvoxel + (dir > 0.0f ? 1 : 0);
            const float3 next_sub = convert_float3(next_subvoxel) * cell_size * inv_dim + cell_min;

            delta *= inv_dim;
            voxel = subvoxel;
            tnext = (next_sub - org) * inv_dir;

            // Inner loop
            while (true) {
                // Lookup leaf
                const int leaf_id = cell_begin + voxel.x + dim * (voxel.y + dim * voxel.z);
                const int2 range = vload2(0, (const global int*)(leaves + leaf_id));

                for (int i = range.x; i < range.y; i++) {
                    const int id = tri_ids[i];
                    const global float* tri_ptr = (global float*)(tris + id);
                    const float4 tri0 = vload4(0, tri_ptr);
                    const float4 tri1 = vload4(1, tri_ptr);
                    const float4 tri2 = vload4(2, tri_ptr);
                    const float3 v0 = tri0.xyz;
                    const float3 e1 = tri1.xyz;
                    const float3 e2 = tri2.xyz;
                    const float3 n = (float3)(tri0.w, tri1.w, tri2.w);

                    const float4 intr = intersect_ray_tri(org, dir, tmin, t, v0, e1, e2, n);
                    if (as_int(intr.x)) {
                        t = intr.y;
                        u = intr.z;
                        v = intr.w;
                        tri_id = id;
                    }
                }

                const float texit = fmin(tnext.x, fmin(tnext.y, tnext.z));
                if (t <= texit) goto end;

                if (texit == tnext.x) {
                    tnext.x += delta.x;
                    voxel.x += vstep.x;
                    if (voxel.x < 0 || voxel.x >= dim) break;

                } else if (texit == tnext.y) {
                    tnext.y += delta.y;
                    voxel.y += vstep.y;
                    if (voxel.y < 0 || voxel.y >= dim) break;
                } else {
                    tnext.z += delta.z;
                    voxel.z += vstep.z;
                    if (voxel.z < 0 || voxel.z >= dim) break;
                }
            }

            // Restore old state
            delta = fabs(cell_size * inv_dir);
            voxel = old_voxel;
            tnext = old_tnext;
        }

        const float texit = fmin(tnext.x, fmin(tnext.y, tnext.z));
        if (t <= texit) break;
        tprev = texit;

        if (texit == tnext.x) {
            tnext.x += delta.x;
            voxel.x += vstep.x;
            if (voxel.x < 0 || voxel.x >= grid_dims.x) break;

        } else if (texit == tnext.y) {
            tnext.y += delta.y;
            voxel.y += vstep.y;
            if (voxel.y < 0 || voxel.y >= grid_dims.y) break;
        } else {
            tnext.z += delta.z;
            voxel.z += vstep.z;
            if (voxel.z < 0 || voxel.z >= grid_dims.z) break;
        }
    }

end:
    {
        global float* hit_ptr = (global float*)(hits + id);
        vstore4((float4)(as_float(tri_id), t, u, v), 0, hit_ptr);
    }
}
