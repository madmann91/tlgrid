#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <cassert>

#include <SDL2/SDL.h>

#include <CL/cl.h>
#include <CL/cl_ext.h>

#include "common.h"
#include "bbox.h"
#include "float3.h"
#include "load_obj.h"

//#define PLANE_ONLY   // Uncomment this to use the plane only test

#define SCREEN_WIDTH  1024
#define SCREEN_HEIGHT 1024

// Top-level grid density
#define TOP_LEVEL_DENSITY 0.5f
// Inner-level grid density
#define INNER_LEVEL_DENSITY 5.0f

struct Grid {
    float3 min;
    int dimx;
    float3 max;
    int dimy;
    float3 cell_size;
    int dimz;
    float3 inv_size;
    int pad;
};

struct Leaf {
    int begin;
    int end;

    Leaf() {}
    Leaf(int a, int b) : begin(a), end(b) {}
};

struct Cell {
    int begin;
    int dim;
};

struct Tri {
    float3 v0;
    float nx;
    float3 e1;
    float ny;
    float3 e2;
    float nz;
};

struct Ray {
    float3 org;
    float tmin;
    float3 dir;
    float tmax;
};

struct Hit {
    int tri_id;
    float t, u, v;
};

struct Ref {
    int tri;
    int cell;

    Ref() {}
    Ref(int tri, int cell)
        : tri(tri), cell(cell)
    {}
};

struct Range {
    int lx; int hx;
    int ly; int hy;
    int lz; int hz;

    Range() {}
    Range(int lx, int hx,
          int ly, int hy,
          int lz, int hz)
        : lx(lx), hx(hx)
        , ly(ly), hy(hy)
        , lz(lz), hz(hz)
    {}
    
    int size() const { return (hx - lx + 1) * (hy - ly + 1) * (hz - lz + 1); }
};

struct Camera {
    float3 eye;
    float3 right;
    float3 up;
    float3 dir;
};

struct View {
    float3 eye;
    float3 forward;
    float3 right;
    float3 up;
    float dist;
    float rspeed;
    float tspeed;
};

typedef std::pair<int, int> MergePair;
typedef std::pair<unsigned, int> HashPair;

inline void compute_grid_dims(const BBox& grid_bb, int num_prims, float d, int* dims) {
    if (num_prims == 0) {
        dims[0] = dims[1] = dims[2] = 0;
    } else {
        const float3 extents = grid_bb.max - grid_bb.min;
        const float v = extents.x * extents.y * extents.z;
        const float r = (cbrt(d * num_prims / v));
        dims[0] = std::max(1, (int)(extents.x * r));
        dims[1] = std::max(1, (int)(extents.y * r));
        dims[2] = std::max(1, (int)(extents.z * r));
    }
}

inline int grid_size(const int* dims) {
    return dims[0] * dims[1] * dims[2];
}

inline int grid_size(const Cell& cell) {
    return cell.dim * cell.dim * cell.dim;
}

inline Camera gen_camera(const float3& eye, const float3& center, const float3& up, float fov, float ratio) {
    Camera cam;
    const float f = tanf(radians(fov / 2));
    cam.dir = normalize(center - eye);
    cam.right = normalize(cross(cam.dir, up)) * (f * ratio);
    cam.up = normalize(cross(cam.right, cam.dir)) * f;
    cam.eye = eye;
    return cam;
}

Range find_coverage(const int* dims, const BBox& bb, const BBox& grid_bb, const float3& inv_size) {
    const int lx = clamp<int>((bb.min.x - grid_bb.min.x) * inv_size.x, 0, dims[0] - 1);
    const int ly = clamp<int>((bb.min.y - grid_bb.min.y) * inv_size.y, 0, dims[1] - 1);
    const int lz = clamp<int>((bb.min.z - grid_bb.min.z) * inv_size.z, 0, dims[2] - 1);

    const int hx = clamp<int>((bb.max.x - grid_bb.min.x) * inv_size.x, 0, dims[0] - 1);
    const int hy = clamp<int>((bb.max.y - grid_bb.min.y) * inv_size.y, 0, dims[1] - 1);
    const int hz = clamp<int>((bb.max.z - grid_bb.min.z) * inv_size.z, 0, dims[2] - 1);

    return Range(lx, hx, ly, hy, lz, hz);
}

inline bool plane_overlap_box(const float3& n, const float3& v0, const float3& min, const float3& max) {
    auto d = dot(n, v0);

    float3 first, last;

    first.x = n.x > 0 ? min.x : max.x;
    first.y = n.y > 0 ? min.y : max.y;
    first.z = n.z > 0 ? min.z : max.z;

    last.x = n.x <= 0 ? min.x : max.x;
    last.y = n.y <= 0 ? min.y : max.y;
    last.z = n.z <= 0 ? min.z : max.z;

    const float d0 = dot(n, first) - d;
    const float d1 = dot(n, last)  - d;
    return d0 * d1 <= 0.0f;
}

inline bool axis_test_x(const float3& half_size,
                        const float3& e, const float3& f,
                        const float3& v0, const float3& v1) {
    const float p0 = e.y * v0.z - e.z * v0.y;
    const float p1 = e.y * v1.z - e.z * v1.y;
    const float rad = f.z * half_size.y + f.y * half_size.z;
    if (std::min(p0, p1) > rad || std::max(p0, p1) < -rad)
        return false;
    return true;
}

inline bool axis_test_y(const float3& half_size,
                        const float3& e, const float3& f,
                        const float3& v0, const float3& v1) {
    const float p0 = e.z * v0.x - e.x * v0.z;
    const float p1 = e.z * v1.x - e.x * v1.z;
    const float rad = f.z * half_size.x + f.x * half_size.z;
    if (std::min(p0, p1) > rad || std::max(p0, p1) < -rad)
        return false;
    return true;
}

inline bool axis_test_z(const float3& half_size,
                        const float3& e, const float3& f,
                        const float3& v0, const float3& v1) {
    const float p0 = e.x * v0.y - e.y * v0.x;
    const float p1 = e.x * v1.y - e.y * v1.x;
    const float rad = f.y * half_size.x + f.x * half_size.y;
    if (std::min(p0, p1) > rad || std::max(p0, p1) < -rad)
        return false;
    return true;
}

inline bool tri_overlap_cell(const BBox& grid_bb, const float3& cell_size, const Tri& tri, int x, int y, int z) {
    const float3 min = grid_bb.min + cell_size * float3(x, y, z);
    const float3 max = grid_bb.min + cell_size * float3(x + 1, y + 1, z + 1);
    const float3 n(tri.nx, tri.ny, tri.nz);
    const float3 v0(tri.v0.x, tri.v0.y, tri.v0.z);
    if (!plane_overlap_box(n, v0, min, max))
        return false;
#ifndef PLANE_ONLY
    float3 e1(tri.e1.x, tri.e1.y, tri.e1.z);
    float3 e2(tri.e2.x, tri.e2.y, tri.e2.z);
    const float3 v1 = v0 - e1;
    const float3 v2 = v0 + e2;

    const float xmin = std::min(v0.x, std::min(v1.x, v2.x));
    const float xmax = std::max(v0.x, std::max(v1.x, v2.x));
    if (xmax < min.x || xmin > max.x) return false;

    const float ymin = std::min(v0.y, std::min(v1.y, v2.y));
    const float ymax = std::max(v0.y, std::max(v1.y, v2.y));
    if (ymax < min.y || ymin > max.y) return false;

    const float zmin = std::min(v0.z, std::min(v1.z, v2.z));
    const float zmax = std::max(v0.z, std::max(v1.z, v2.z));
    if (zmax < min.z || zmin > max.z) return false;

    const float3 center    = (max + min) * 0.5f;
    const float3 half_size = (max - min) * 0.5f;

    const float3 w0 = v0 - center;
    const float3 w1 = v1 - center;
    const float3 w2 = v2 - center;

    const float3 f1(fabsf(e1.x), fabsf(e1.y), fabsf(e1.z));
    if (!axis_test_x(half_size, e1, f1, w0, w2)) return false;
    if (!axis_test_y(half_size, e1, f1, w0, w2)) return false;
    if (!axis_test_z(half_size, e1, f1, w1, w2)) return false;

    const float3 f2(fabsf(e2.x), fabsf(e2.y), fabsf(e2.z));
    if (!axis_test_x(half_size, e2, f2, w0, w1)) return false;
    if (!axis_test_y(half_size, e2, f2, w0, w1)) return false;
    if (!axis_test_z(half_size, e2, f2, w1, w2)) return false;

    const float3 e3 = e1 + e2;

    const float3 f3(fabsf(e3.x), fabsf(e3.y), fabsf(e3.z));
    if (!axis_test_x(half_size, e3, f3, w0, w2)) return false;
    if (!axis_test_y(half_size, e3, f3, w0, w2)) return false;
    if (!axis_test_z(half_size, e3, f3, w0, w1)) return false;
#endif
    return true;
}

bool build_grid(const std::vector<Tri>& tris, int* dims, BBox& grid_bb,
                cl_context ctx, cl_mem& grid_leaves, cl_mem& grid_cells, cl_mem& grid_ids, cl_mem& grid_tris) {
    auto t0 = std::chrono::high_resolution_clock::now();
    std::vector<BBox> bboxes(tris.size());
    grid_bb = BBox::empty();

    int tri_count = tris.size();

    // Compute bounding boxes and global bounding box
    for (int i = 0; i < tri_count; i++) {
        const auto& tri = tris[i];
        const float3 v0(tri.v0.x, tri.v0.y, tri.v0.z);
        const float3 e1(tri.e1.x, tri.e1.y, tri.e1.z);
        const float3 e2(tri.e2.x, tri.e2.y, tri.e2.z);
        auto v1 = v0 - e1;
        auto v2 = v0 + e2;

        bboxes[i].min = min(v0, min(v1, v2));
        bboxes[i].max = max(v0, max(v1, v2));

        #pragma omp critical
        {
            grid_bb.extend(bboxes[i]);
        }
    }

    compute_grid_dims(grid_bb, tris.size(), TOP_LEVEL_DENSITY, dims);

    // Compute the number of references
    const float3 grid_extents = grid_bb.max - grid_bb.min;
    const float3 cell_size(grid_extents.x / dims[0], grid_extents.y / dims[1], grid_extents.z / dims[2]);
    const float3 inv_size(dims[0] / grid_extents.x, dims[1] / grid_extents.y, dims[2] / grid_extents.z);

    const int dim0  = dims[0];
    const int dim01 = dims[1] * dims[0];

    // Put the references in the array
    std::vector<Ref> refs;
    for (int i = 0; i < tri_count; i++) {
        const auto& range = find_coverage(dims, bboxes[i], grid_bb, inv_size);

        // Examine each cell and determine if the triangle is really inside
        for (int x = range.lx; x <= range.hx; x++) {
            for (int y = range.ly; y <= range.hy; y++) {
                for (int z = range.lz; z <= range.hz; z++) {
                    if (tri_overlap_cell(grid_bb, cell_size, tris[i], x, y, z))
                        refs.emplace_back(i, x + dim0 * y + dim01 * z);
                }
            }
        }
    }

    int ref_count = refs.size();

    // Sort the references
    std::sort(refs.begin(), refs.end(), [] (const Ref& a, const Ref& b) {
        return a.cell < b.cell;
    });

    std::vector<Cell> cells(grid_size(dims));

    // Create the grid cells
    #pragma omp parallel for
    for (int i = 0; i < cells.size(); i++) {
        cells[i].begin = 0;
    }

    // Compute the number of references in each cell
    #pragma omp parallel for
    for (int i = 0; i < ref_count; i++) {
        #pragma omp atomic
        cells[refs[i].cell].begin++;
    }

    // Compute the resolution of each subgrid
    #pragma omp parallel for
    for (int z = 0; z < dims[2]; z++) {
        int k = z * dim01;
        for (int y = 0; y < dims[1]; y++) {
            for (int x = 0; x < dims[0]; x++) {
                const float3 min = grid_bb.min + cell_size * float3(x, y, z);
                const float3 max = grid_bb.min + cell_size * float3(x + 1, y + 1, z + 1);
                auto& c = cells[k];
                int cdims[3];
                compute_grid_dims(BBox(min, max), c.begin, INNER_LEVEL_DENSITY, cdims);
                c.dim = std::max(cdims[0], std::max(cdims[1], cdims[2]));
                k++;
            }
        }
    }

    // Compute the position of each leaf
    cells[0].begin = 0;
    for (int i = 1; i < cells.size(); i++) {
        cells[i].begin = cells[i - 1].begin + grid_size(cells[i - 1]);
    }

    // Emit the new references
    std::vector<Ref> new_refs;
    for (int i = 0; i < refs.size(); i++) {
        const Cell& cell = cells[refs[i].cell];

        const int cz =  refs[i].cell / dim01;
        const int cy = (refs[i].cell / dim0) % dims[1];
        const int cx = (refs[i].cell % dim0);
        const int r = refs[i].tri;

        const float3 subcell_size = cell_size / float3(cell.dim);
        const float3 subcell_inv  = float3(cell.dim) / cell_size;
        const BBox cell_bb(grid_bb.min + cell_size * float3(cx, cy, cz),
                           grid_bb.min + cell_size * float3(cx + 1, cy + 1, cz + 1));

        const int cdims[3] = { cell.dim, cell.dim, cell.dim };
        const auto& range = find_coverage(cdims, bboxes[r], cell_bb, subcell_inv);

        // Examine each sub-cell and determine if the triangle is really inside
        for (int x = range.lx; x <= range.hx; x++) {
            for (int y = range.ly; y <= range.hy; y++) {
                for (int z = range.lz; z <= range.hz; z++) {
                    if (tri_overlap_cell(cell_bb, subcell_size, tris[r], x, y, z))
                        new_refs.emplace_back(r, cell.begin + x + cell.dim * (y + cell.dim * z));
                }
            }
        }
    }

    std::sort(new_refs.begin(), new_refs.end(), [] (const Ref& a, const Ref& b) {
        return a.cell < b.cell;
    });

    std::vector<Leaf> leaves(cells.back().begin + grid_size(cells.back()), Leaf(0, 0));
    // Look for neighbouring references with different leaves
    #pragma omp parallel for
    for (int i = 0; i < new_refs.size(); i++) {
        const int c0 = new_refs[i].cell;
        if (i == new_refs.size() - 1) {
            leaves[c0].end = i + 1;
        } else {
            const int c1 = new_refs[i + 1].cell;
            if (c0 != c1) {
                leaves[c0].end = i + 1;
                leaves[c1].begin = i + 1;
            }
        }
    }

    // Emit triangle ids
    std::vector<int> ref_ids(new_refs.size());
    #pragma omp parallel for
    for (int i = 0; i < new_refs.size(); i++) {
        ref_ids[i] = new_refs[i].tri;
    }

    auto t1 = std::chrono::high_resolution_clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();

    // Create opencl buffers
    const cl_mem_flags mem_flags = CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR /* | CL_MEM_HOST_WRITE_ONLY */;
    cl_int err = CL_SUCCESS;
    grid_leaves = clCreateBuffer(ctx, mem_flags, sizeof(Leaf) * leaves.size(), leaves.data(), &err);
    if (err != CL_SUCCESS) return false;
    grid_cells = clCreateBuffer(ctx, mem_flags, sizeof(Cell) * cells.size(), cells.data(), &err);
    if (err != CL_SUCCESS) return false;
    grid_ids = clCreateBuffer(ctx, mem_flags, sizeof(int) * ref_ids.size(), ref_ids.data(), &err);
    if (err != CL_SUCCESS) return false;
    grid_tris = clCreateBuffer(ctx, mem_flags, sizeof(Tri) * tris.size(), (void*)tris.data(), &err);
    if (err != CL_SUCCESS) return false;

    std::cout << "Grid built in " << ms << " ms ("
              << dims[0] << "x" << dims[1] << "x" << dims[2] << ", "
              << cells.size() << " cells, " << leaves.size() << " leaves, "
              << ref_count << " references)" << std::endl;
    const size_t total_mem = cells.size() * sizeof(cells[0]) +
                             leaves.size() * sizeof(leaves[0]) +
                             ref_ids.size() * sizeof(ref_ids[0]) +
                             tris.size() * sizeof(tris[0]);
    std::cout << "Total memory: " << (double)total_mem / (double)(1024 * 1024) << " MB" << std::endl;

    return true;
}

void flush_events() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {}
}

bool handle_events(View& view) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                return true;
            case SDL_MOUSEMOTION:
                if (SDL_GetMouseState(nullptr, nullptr) & SDL_BUTTON(SDL_BUTTON_LEFT)) {
                    view.right = cross(view.forward, view.up);
                    view.forward = rotate(view.forward, view.right, -event.motion.yrel * view.rspeed);
                    view.forward = rotate(view.forward, view.up,    -event.motion.xrel * view.rspeed);
                    view.forward = normalize(view.forward);
                    view.up = normalize(cross(view.right, view.forward));
                }
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_UP:    view.eye = view.eye + view.tspeed * view.forward; break;
                    case SDLK_DOWN:  view.eye = view.eye - view.tspeed * view.forward; break;
                    case SDLK_LEFT:  view.eye = view.eye - view.tspeed * view.right;   break;
                    case SDLK_RIGHT: view.eye = view.eye + view.tspeed * view.right;   break;
                    case SDLK_KP_PLUS:  view.tspeed *= 1.1f; break;
                    case SDLK_KP_MINUS: view.tspeed /= 1.1f; break;
                    case SDLK_c:
                        {
                            const float3 center = view.eye + view.forward * view.dist;
                            std::cout << "Eye: " << view.eye.x << " " << view.eye.y << " " << view.eye.z << std::endl;
                            std::cout << "Center: " << center.x << " " << center.y << " " << center.z << std::endl;
                            std::cout << "Up: " << view.up.x << " " << view.up.y << " " << view.up.z << std::endl;
                        }
                        break;
                    case SDLK_ESCAPE:
                        return true;
                }
                break;
        }
    }
    return false;
}

bool load_model(const std::string& file_name, std::vector<Tri>& model) {
    obj::File obj_file;
    if (!load_obj(file_name, obj_file))
        return false;

    for (auto& object : obj_file.objects) {
        for (auto& group : object.groups) {
            for (auto& face : group.faces) {
                const float3& v0 = obj_file.vertices[face.indices[0].v];
                for (int i = 0; i < face.index_count - 2; i++) {
                    const float3& v1 = obj_file.vertices[face.indices[i + 1].v];
                    const float3& v2 = obj_file.vertices[face.indices[i + 2].v];
                    const float3 e1 = v0 - v1;
                    const float3 e2 = v2 - v0;
                    const float3 n  = cross(e1, e2);

                    const Tri tri = {
                        v0, n.x,
                        e1, n.y,
                        e2, n.z
                    };
                    model.push_back(tri);
                }
            }
        }
    }

    std::cout << model.size() << " triangles" << std::endl;
    return true;
}

void usage() {
    std::cout << "Usage: tlgrid [options] 3d-model-file" << std::endl;
    std::cout << "Available options:\n"
              << "    -h    --help             Show this message.\n"
              << "    -c    --clip             Set the clipping distance.\n";
}

int main(int argc, char** argv) {
    if (sizeof(Ray)  != 8 * sizeof(float)  ||
        sizeof(Hit)  != 4 * sizeof(float)  ||
        sizeof(Leaf) != 2 * sizeof(float)  ||
        sizeof(Cell) != 2 * sizeof(float)  ||
        sizeof(Tri)  != 12 * sizeof(float) ||
        sizeof(Grid) != 16 * sizeof(float)) {
        std::cerr << "The sizes of the structures do not match.\n"
                  << "Please recompile with the correct alignment options." << std::endl;
        return 1;
    }

    if (argc < 2) {
        usage();
        return 1;
    }

    std::string file_name;
    float clip = 50.0f;
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] == '-') {
            if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
                usage();
                return 0;
            } else if (!strcmp(argv[i], "-c") || !strcmp(argv[i], "--clip")) {
                if (i + 1 >= argc) {
                    std::cerr << "Missing argument for: " << argv[i] << std::endl;
                    return 1;
                }
                clip = strtof(argv[i + 1], nullptr);
                i++;
            } else {
                std::cerr << "Unknown option: " << argv[i] << std::endl;
                return 1;
            }
        } else {
            if (file_name.length() > 0) {
                std::cerr << "Model file name specified twice." << std::endl;
                return 1;
            }
            file_name = argv[i];
        }
    }

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Cannot initialize SDL." << std::endl;        
        return 1;
    }

    // Enumerate platforms
    cl_uint num_platforms;
    clGetPlatformIDs(0, nullptr, &num_platforms);
    std::vector<cl_platform_id> platforms(num_platforms);
    clGetPlatformIDs(num_platforms, platforms.data(), nullptr);

    // Print platform information
    std::cout << "Platforms:\n";
    for (cl_uint i = 0; i < num_platforms; i++) {
        size_t len;
        clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 0, nullptr, &len);
        std::string profile(len, 0);
        clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, len, &profile[0], nullptr);
        std::cout << "(" << i << ") " << profile << std::endl;
    }

    unsigned platform_index;
    std::cout << "Which platform should be used (-1 to quit)? ";
    std::cin >> platform_index;

    if (platform_index >= num_platforms) {
        std::cerr << "Invalid platform index." << std::endl;
        return 1;
    }

    // Enumerate devices
    cl_uint num_devices;
    clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_GPU, 0, nullptr, &num_devices);
    if (num_devices == 0) {
        std::cerr << "There is no GPU in the selected platform." << std::endl;
        return 1;
    }

    std::vector<cl_device_id> devices(num_devices);
    clGetDeviceIDs(platforms[platform_index], CL_DEVICE_TYPE_GPU, num_devices, devices.data(), nullptr);

    cl_context_properties properties[] =
        { CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[platform_index]), 0};

    cl_int err;
    cl_context ctx = clCreateContext(properties, num_devices, devices.data(), nullptr, nullptr, &err);
    if (err != CL_SUCCESS) {
        std::cerr << "Cannot create OpenCL context." << std::endl;
        return 1;
    }
    cl_command_queue queue = clCreateCommandQueue(ctx, devices[0], CL_QUEUE_PROFILING_ENABLE, &err);
    if (err != CL_SUCCESS) {
        std::cerr << "Cannot create command queue." << std::endl;
        return 1;
    }

    std::ifstream src_file("grid.cl");
    if (!src_file) {
        std::cerr << "Cannot open kernel source file." << std::endl;
        return 1;
    }

    std::string src_string = std::string(std::istreambuf_iterator<char>(src_file), (std::istreambuf_iterator<char>()));
    const char* source = src_string.c_str();
    const size_t len = src_string.length();
    cl_program program = clCreateProgramWithSource(ctx, 1, &source, &len, nullptr);
    const char* options = "-cl-fast-relaxed-math";

    if (clBuildProgram(program, 1, devices.data(), options, nullptr, nullptr) != CL_SUCCESS) {
        size_t log_size;
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, 0, nullptr, &log_size);
        std::string log_str(log_size + 1, 0);
        clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, log_size, &log_str[0], nullptr);
        std::cerr << "Compilation failed:\n";
        std::cerr << log_str;
        return 1;
    }

    cl_kernel kernel = clCreateKernel(program, "traverse_grid", &err);
    if (err != CL_SUCCESS) {
        std::cerr << "Cannot create kernel." << std::endl;
        return 1;
    }

    std::vector<Tri> model;
    if (!load_model(file_name, model)) {
        std::cerr << "Cannot load model." << std::endl;
        return 1;
    }

    const int w = SCREEN_WIDTH, h = SCREEN_HEIGHT;
    SDL_Window* win = SDL_CreateWindow("TLGrid", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w, h, 0);
    SDL_Surface* screen = SDL_GetWindowSurface(win);

    BBox grid_bb;
    int dims[3];
    cl_mem leaves, cells, tri_ids, tris;

    // Build the grid for the model
    if (!build_grid(model, dims, grid_bb, ctx, leaves, cells, tri_ids, tris)) {
        std::cerr << "Cannot build grid." << std::endl;
        return 1;
    }

    cl_mem rays = clCreateBuffer(ctx, CL_MEM_READ_ONLY /* | CL_MEM_HOST_WRITE_ONLY */, sizeof(Ray) * w * h, nullptr, &err);
    if (err != CL_SUCCESS) {
        std::cerr << "Cannot create buffer for rays." << std::endl;
        return 1;
    }
    cl_mem hits = clCreateBuffer(ctx, CL_MEM_WRITE_ONLY /* | CL_MEM_HOST_READ_ONLY */, sizeof(Hit) * w * h, nullptr, &err);
    if (err != CL_SUCCESS) {
        std::cerr << "Cannot create buffer for hit points." << std::endl;
        return 1;
    }

    const float3 extents = grid_bb.max - grid_bb.min;
    Grid grid_info = {
        grid_bb.min, dims[0],
        grid_bb.max, dims[1],
        extents / float3(dims[0], dims[1], dims[2]), dims[2],
        float3(dims[0], dims[1], dims[2]) / extents, 0
    };

    cl_mem grid = clCreateBuffer(ctx, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(Grid), &grid_info, &err);
    if (err != CL_SUCCESS) {
        std::cerr << "Cannot create constant memory buffer for grid parameters." << std::endl;
        return 1;
    }

    const float fov = 60.0f;
    /*Camera cam = gen_camera(float3(0.0f, 0.0f, -10.0f),
                            float3(0.0f, 0.0f, 0.0f),
                            float3(0.0f, 1.0f, 0.0f),
                            fov, (float)w / (float)h);*/
    // Sponza
    /*Camera cam = gen_camera(float3(-1140.7, 222.835, -34.9641),
                            float3(-1040.73, 220.572, -36.0312),
                            float3(0.0, 1.0, 0.0),
                            fov, (float)w / (float)h);*/
    // Sibenik
    Camera cam = gen_camera(float3(10.4209, -0.270721, -0.058378),
                            float3(-80.3837, -42.0259, -3.38165),
                            float3(-0.418404, 0.907921, 0.0),
                            fov, (float)w / (float)h);
    /*Camera cam = gen_camera(float3(12.3236, -12.598, 1.22812),
                            float3(-87.4545, -18.3619, -2.1059),
                            float3(0.0, 1, 0.0),
                            fov, (float)w / (float)h);*/
    // Conference
    /*Camera cam = gen_camera(float3(-39.6258, 18.8307, -0.455114),
                            float3(57.4049, -5.3522, -0.935061),
                            float3(0.0, 0.0, 1.0),
                            fov, (float)w / (float)h);*/
    View view = {
        cam.eye,                 // Eye
        normalize(cam.dir),      // Forward
        normalize(cam.right),    // Right
        normalize(cam.up),       // Up
        100.0f, 0.005f, 1.0f     // View distance, rotation speed, translation speed
    };

    flush_events();

    long ktime = 0;
    int frames = 0;
    int ticks = SDL_GetTicks();
    bool done = false;

    while (!done) {
        // Generate primary rays
        Ray* host_rays = (Ray*)clEnqueueMapBuffer(queue, rays,
            true, CL_MAP_WRITE /*| CL_MAP_WRITE_INVALIDATE_REGION*/,
            0, w * h * sizeof(Ray),
            0, nullptr, nullptr, &err);
        if (err != CL_SUCCESS) {
            std::cerr << "Cannot map ray buffer" << std::endl;
            return 1;
        }
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                const float kx = 2 * x / (float)w - 1;
                const float ky = 1 - 2 * y / (float)h;
                const float3 dir = cam.dir + cam.right * kx + cam.up * ky;

                auto& ray = host_rays[y * w + x];
                ray.org = cam.eye;
                ray.dir = dir;
                ray.tmin = 0.0f;
                ray.tmax = clip;
            }
        }
        cl_event rays_event;
        clEnqueueUnmapMemObject(queue, rays, host_rays, 0, nullptr, &rays_event);
        clWaitForEvents(1, &rays_event);

        if (SDL_GetTicks() - ticks >= 1000) {
            std::ostringstream caption;
            caption << "TLGrid [" << (double)((long)frames * w * h) * 1000.0f / ktime << " MRays/s]";
            SDL_SetWindowTitle(win, caption.str().c_str());
            ticks = SDL_GetTicks();
            ktime = 0;
            frames = 0;
        }

        // Setup kernel arguments
        const int ray_count = w * h;
        clSetKernelArg(kernel, 0, sizeof(cl_mem), &grid);
        clSetKernelArg(kernel, 1, sizeof(cl_mem), &leaves);
        clSetKernelArg(kernel, 2, sizeof(cl_mem), &cells);
        clSetKernelArg(kernel, 3, sizeof(cl_mem), &tri_ids);
        clSetKernelArg(kernel, 4, sizeof(cl_mem), &tris);
        clSetKernelArg(kernel, 5, sizeof(cl_mem), &rays);
        clSetKernelArg(kernel, 6, sizeof(cl_mem), &hits);
        clSetKernelArg(kernel, 7, sizeof(int), &ray_count);

        const int block_size = 32;
        const size_t global_offset[3] = { 0, 0, 0 };
        const size_t global_dims[3] = { ray_count, 1, 1 };
        const size_t local_dims[3] = { block_size, 1, 1 };

        // Run the traversal kernel
        cl_event kernel_event;
        err = clEnqueueNDRangeKernel(queue, kernel, 2, global_offset, global_dims, local_dims, 0, nullptr, &kernel_event);
#ifndef NDEBUG
        if (err == CL_INVALID_KERNEL)  std::cerr << "Invalid kernel." << std::endl;
        if (err == CL_INVALID_CONTEXT) std::cerr << "Invalid context." << std::endl;
        if (err == CL_INVALID_KERNEL_ARGS) std::cerr << "Invalid kernel arguments." << std::endl;
        if (err == CL_INVALID_WORK_DIMENSION) std::cerr << "Invalid work dimensions." << std::endl;
        if (err == CL_INVALID_GLOBAL_OFFSET) std::cerr << "Invalid global offset." << std::endl;
        if (err == CL_INVALID_WORK_GROUP_SIZE) std::cerr << "Invalid work group size." << std::endl;
        if (err == CL_INVALID_WORK_ITEM_SIZE) std::cerr << "Invalid work item size." << std::endl;
        if (err == CL_MISALIGNED_SUB_BUFFER_OFFSET) std::cerr << "Invalid sub buffer offset." << std::endl;
        if (err == CL_INVALID_IMAGE_SIZE) std::cerr << "Invalid image size." << std::endl;
        if (err != CL_SUCCESS) return 1;
#else
        if (err != CL_SUCCESS) {
            std::cerr << "Kernel launch failure" << std::endl;
            return 1;
        }
#endif

        clWaitForEvents(1, &kernel_event);

        // Get execution time
        cl_ulong t0, t1;
        clGetEventProfilingInfo(kernel_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &t0, nullptr);
        clGetEventProfilingInfo(kernel_event, CL_PROFILING_COMMAND_END,   sizeof(cl_ulong), &t1, nullptr);
        ktime += t1 - t0;

        // Copy image to screen
        Hit* host_hits = (Hit*)clEnqueueMapBuffer(queue, hits,
            true, CL_MAP_READ,
            0, w * h * sizeof(Hit),
            0, nullptr, nullptr, &err);
        if (err != CL_SUCCESS) {
            std::cerr << "Cannot map result buffer" << std::endl;
            return 1;
        }
        SDL_LockSurface(screen);
        for (int y = 0, my = std::min(screen->h, h); y < my; y++) {
            char* row = (char*)screen->pixels + screen->pitch * y;
            for (int x = 0, mx = std::min(screen->w, w); x < mx; x++) {
                const char color = host_hits[y * w + x].t * 255.0f / clip;
                row[x * 4 + 0] = color;
                row[x * 4 + 1] = color;
                row[x * 4 + 2] = color;
                row[x * 4 + 3] = 255;
            }
        }
        SDL_UnlockSurface(screen);
        cl_event hits_event;
        clEnqueueUnmapMemObject(queue, hits, host_hits, 0, nullptr, &hits_event);
        clWaitForEvents(1, &hits_event);

        SDL_UpdateWindowSurface(win);

        done = handle_events(view);
        cam = gen_camera(view.eye, view.eye + view.forward * view.dist, view.up, fov, (float)w / (float)h);
        frames++;
    }

    clReleaseContext(ctx);
    SDL_DestroyWindow(win);
    SDL_Quit();

    return 0;
}

